<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    //used constants only when we have enum type for gender in database
    const MALE      = 'male';
    const FEMALE    = 'female';

    public function country()
    {
        return $this->belongsTo('App\Country', 'country_id', 'id');
    }

    public function transactions()
    {
        return $this->hasMany('App\Transaction', 'customer_id', 'id');
    }
}
