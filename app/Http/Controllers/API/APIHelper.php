<?php
/**
 * Created by PhpStorm.
 * User: sebas
 * Date: 3/21/2017
 * Time: 8:49 PM
 */

namespace App\Http\Controllers\API;


class APIHelper
{
    private static $_model;

    public static function setData( $fields, $request) {

        foreach($fields as $field) {
            if(isset($request[$field]))
                self::$_model->{$field} = $request[$field];
        }

        return self::$_model;
    }

    public static function forModel($model) {
        self::$_model = $model;

        $instance = new static;

        return $instance;
    }
}