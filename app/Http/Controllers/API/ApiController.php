<?php
/**
 * Created by PhpStorm.
 * User: sebas
 * Date: 3/21/2017
 * Time: 7:56 PM
 */

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;

class ApiController extends Controller
{
    private $_success   = 'success';
    private $_error     = 'error';

    protected function success($extra = null) {
        $response = ['status' => $this->_success];

        !is_array($extra) ? : $response = array_merge($response, $extra);

        return response()->json($response);
    }

    protected function error($extra = null) {
        $response = ['status' => $this->_error];

        !is_array($extra) ? : $response = array_merge($response, $extra);

        return response()->json($response);
    }

}