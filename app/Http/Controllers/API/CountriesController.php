<?php

namespace App\Http\Controllers\API;

use App\Country;
use Illuminate\Http\Request;
use App\Http\Requests\CountryCreate;

class CountriesController extends ApiController
{
    public function create(CountryCreate $request)
    {
        $country = new Country();

        $country->code = $request->get('code');
        $country->name = $request->get('name');

        if(!$country->save())
            return $this->error(['message' => 'error_saving_db']);

        return $this->success();
    }
}
