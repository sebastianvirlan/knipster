<?php

namespace App\Http\Controllers\API;

use App\Country;
use App\Customer;
use Illuminate\Http\Request;
use App\Http\Requests\CustomerCreate;
use App\Http\Controllers\API\APIHelper;
class CustomersController extends ApiController
{
    public function create(CustomerCreate $request)
    {

        $country    = Country::where('code', $request->get('country'))->first();

        $customer   = new Customer();

        $customer->email        = $request->get('email');
        $customer->first_name   = $request->get('first_name');
        $customer->last_name    = $request->get('last_name');
        $customer->country_id   = $request->get('email');
        $customer->country_id   = $country->id;
        $customer->bonus        = rand(5, 20);

        if(!$customer->save())
            return $this->error(['message' => 'error_saving_db']);

        return $this->success();
    }

    public function update(Request $request, $id) {

        $country    = Country::where('code', $request->get('country'))->first();

        $customer   = Customer::find($id);

        $customer = APIHelper::forModel($customer)
                            ->setData(['email', 'first_name', 'last_name', 'gender'], $request->all());

        $customer->country_id   = $country->id;

        if(!$customer->save())
            return $this->error(['message' => 'error_saving_db']);

        return $this->success();
    }
}
