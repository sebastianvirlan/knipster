<?php

namespace App\Http\Controllers\API;

use App\Country;
use App\Transaction;
use Illuminate\Http\Request;
use Carbon\Carbon;
use DB;

class ReportsController extends ApiController
{
    public function index(Request $request, $days = 7)
    {

        //@TO-DO some refactor here
        
        for($i = 0; $i <= $days - 1; $i++) {
            $date = new Carbon;
            $date->subDays($i);
            $from = $date->toDateString();

            $countries = Country::whereHas('customers.transactions', function($query) use($from){
                $query->whereDate('created_at', $from);
            })->get();

            foreach($countries as $country) {

                $depositFromDate    = Transaction::whereDate('created_at', $from)->where('type', Transaction::DEPOSIT);
                $withdrawFromDate   = Transaction::whereDate('created_at', $from)->where('type', Transaction::WITHDRAW);

                $response['report'][$i]['date']               = $from;
                $response['report'][$i]['unique_customers']   = count(Transaction::whereDate('created_at', $from)->groupBy('customer_id')->get());
                $response['report'][$i]['country']            = $country->code;
                $response['report'][$i]['deposits']           = $depositFromDate->count();
                $response['report'][$i]['deposit_amount']     = $depositFromDate->sum('amount');
                $response['report'][$i]['withdraws']          = $withdrawFromDate->count();
                $response['report'][$i]['withdraw_amount']    = $withdrawFromDate->sum('amount');
            }
        }

        return response()->json($response);
    }
}
