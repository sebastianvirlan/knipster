<?php

namespace App\Http\Controllers\API;

use App\Bonus;
use App\Customer;
use App\Transaction;
use Illuminate\Http\Request;
use App\Http\Requests\TransactionCreate;

class TransactionsController extends ApiController
{

    public function create(TransactionCreate $request)
    {

        $amount     = $request->get('amount');
        $user       = Customer::find($request->get('customer_id'));

        if($request->get('type') == Transaction::DEPOSIT)
            return $this->processDeposit($user->id, $user->bonus, $amount);

        if($request->get('type') == Transaction::WITHDRAW)
            return $this->processWithdraw($user->id, $amount);

    }

    private function processDeposit($userID, $bonus, $amount)
    {

        $transaction = $this->makeTransaction([
            'customer_id'   => $userID,
            'amount'    => $amount,
            'type'      => Transaction::DEPOSIT
        ]);

        if(!$transaction)
            return $this->error(['message' => 'error_saving_db']);

        $userTransactionsCount = Transaction::where('customer_id', $userID)->count();

        if ($userTransactionsCount % 3 == 0)
            $this->addBonus($transaction, $amount, $bonus);

        return $this->success();
    }

    private function addBonus($transaction, $amount, $bonus)
    {
        $amount = ($amount * $bonus) / 100;
        $transaction->bonus()->create(['amount' => $amount]);

    }

    private function processWithdraw($userID, $amount)
    {
        $userTotalDeposit   = Transaction::totalDepositForUser($userID);
        $userTotalWithdraw  = Transaction::totalWithdrawForUser($userID);

        $currentBalance = $userTotalDeposit - $userTotalWithdraw;

        if ($amount > Transaction::MAX_WITHDRAW)
            return $this->error(['message' => 'Maximum of '.Transaction::MAX_WITHDRAW .' exceeded.']);

        if ($amount > $currentBalance)
            return $this->error(['message' => 'Youn don`t have this amount in account']);

        $transaction = $this->makeTransaction([
            'customer_id'   => $userID,
            'amount'    => $amount,
            'type'      => Transaction::WITHDRAW
        ]);

        if(!$transaction)
            return $this->error(['message' => 'error_saving_db']);
    }

    private function makeTransaction($data)
    {
        $transaction = new Transaction();

        $transaction->customer_id   = $data['customer_id'];
        $transaction->amount        = $data['amount'];
        $transaction->type          = $data['type'];

        if(!$transaction->save())
            return false;

        return $transaction;
    }

}
