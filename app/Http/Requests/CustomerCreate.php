<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CustomerCreate extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function wantsJson()
    {
        return true;
    }
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email'         => 'required:unique:customers,email|email',
            'first_name'    => 'required|min:3|max:255',
            'last_name'     => 'required|min:3|max:255',
            'gender'        => 'required|in:male,female',
            'country'       => 'required|exists:countries,code'
        ];
    }
}
