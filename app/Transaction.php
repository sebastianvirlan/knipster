<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    //used constants only when we have enum type for gender in database
    const DEPOSIT       = 'deposit';
    const WITHDRAW      = 'withdraw';
    const MAX_WITHDRAW  = 100;

    public function customer()
    {
        return $this->belongsTo('App\Customer', 'customer_id', 'id');
    }

    public function bonus()
    {
        return $this->hasOne('App\Bonus', 'transaction_id', 'id');
    }

    public static function totalDepositForUser($userID)
    {
        return self::where('customer_id', $userID)
            ->where('type', self::DEPOSIT)
            ->sum('amount');
    }

    public static function totalWithdrawForUser($userID)
    {
        return self::where('customer_id', $userID)
            ->where('type', Transaction::WITHDRAW)
            ->sum('amount');
    }
}
