#Knipster Test

1. Clone the application
2. Copy .env.example into .env
3. Run `php artisan key:generate`
4. Prepare your .env file for a MySql Connection
5. Run `php artisan migrate`
6. Run `php artisan serve`

#ENDPOINTS

`POST: api/country`
Params: `code=DE&name=Germany` , fields are required

`POST: api/customer`
Params: `first_name=Sebastian&country=RO&last_name=Vîrlan&email=sebastian.virlan@email.com&gender=male`, fields are required

`PUT: api/customer/ID`
Params: `first_name=Sebastian&country=RO&last_name=Vîrlan&email=sebastian.virlan@email.com&gender=male`

`GET: api/report/DAYS?`

To run tests:

Ubuntu: `phpunit`
Windows: `vendor\bin\phpunit`