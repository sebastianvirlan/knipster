<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['middleware' => 'api'], function () {

    // Customer
    Route::put('/customer/{id}', 'API\CustomersController@update');
    Route::post('/customer', 'API\CustomersController@create');

    // Country
    Route::post('/country', 'API\CountriesController@create');

    //Transaction
    Route::post('/transaction', 'API\TransactionsController@create');

    //Reports
    Route::get('/report/{days?}', 'API\ReportsController@index');

});

