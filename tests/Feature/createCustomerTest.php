<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class createCustomerTest extends TestCase
{
    /**
     * Customer insert.
     *
     * @return void
     */
    public function testCustomerCreate()
    {


        $customer = [

            'email'         => 'sebastian.virlan.test+'.rand(10, 50).'@yahoo.com',
            'first_name'    => 'Sebastian',
            'last_name'     => 'Vîrlan',
            'gender'        => 'male',
            'country'       =>  'RO'

        ];


        $response = $this->post('api/customer', $customer)->json();
        
        $this->assertEquals('success', $response['status']);




        //$response = $this->call('POST', 'api/customer', $customer);
        //$data = json_decode($response);
        //$this->assertEquals('error', $data->status);
    }

    public function testListRaport()
    {
        $response = $this->get('api/report')->json();

        $this->assertArrayHasKey('report', $response);
    }
}
